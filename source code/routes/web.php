<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// các route của người dùng chưa đăng nhập (hoặc đã đăng nhập)

Route::get('/', function () {
    return view('index');
})->name('home');

Route::get('/hotel/search', 'HotelController@search');

Route::get('/hotel/{city}', 'HotelController@getHotelByCity');

Route::post('hotel/filter', 'HotelController@filterHotel');

Route::post('hotel/sort', 'HotelController@sortHotel');

Route::post('hotel/dss', 'HotelController@getDSS');

Route::get('/hotel/detail/{id}', 'HotelController@getHotelDetailById');

Route::post('room/update', 'RoomController@updateStatusRoom');

// route phụ trách login và logout

Route::get('/login', function () {
   return view('login');
})->name('login');

Route::post('checklogin', 'UserController@checkLogin');

Route::get('logout', ['as'=>'logout', function(){
    session()->flush();
    return redirect()->route('home');
}])->middleware('checklogin');

// các route của user đã đăng nhập

Route::post('/review/addreview', 'ReviewController@addReview')->middleware('checklogin');

Route::get('room/book/{id}', 'BookController@showBookingPage')->middleware('checklogin');

Route::post('book/sendrequest', 'BookController@sendRequest')->middleware('checklogin');

Route::get('book/getresponse', 'BookController@getResponse')->middleware('checklogin');

Route::get('profile/{id}', 'UserController@showProfile')->middleware('checklogin');

Route::post('user/update', 'UserController@updateProfile')->middleware('checklogin');

Route::post('favorite/delete', 'FavoriteController@delete')->middleware('checklogin');

Route::post('favorite/add', 'FavoriteController@add')->middleware('checklogin');

Route::post('user/changepassword', 'UserController@changePassword')->middleware('checklogin');

// các route của admin

Route::get('admin', 'AdminController@showAdminPage')->middleware('checkadmin');

Route::post('user/updaterole', 'UserController@updateRole')->middleware('checkadmin');

Route::post('user/updatestatus', 'UserController@updateStatus')->middleware('checkadmin');

Route::post('hotel/delete', 'HotelController@deleteHotel')->middleware('checkadmin');

Route::post('hotel/updatehotel', 'HotelController@updateHotel')->middleware('checkadmin');

Route::post('hotel/addhotel', 'HotelController@addHotel')->middleware('checkadmin');
